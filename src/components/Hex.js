import React from 'react'
import './Hex.css'

import iconKnife from '../assets/icons/icon_knife.svg'
import iconCup from '../assets/icons/icon_cup.svg'


function Hex() {
    return (
        <div className="hex__container">
            <div className="hex__wrap">
                <img src={iconKnife} />
                <span className="hex__text">Morning</span>
            </div>
            <div className="hex__wrap">
                <img src={iconKnife} />
                <span className="hex__text">Lunch</span>
            </div>
            <div className="hex__wrap">
                <img src={iconKnife} />
                <span className="hex__text">Dinner</span>
            </div>
            <div className="hex__wrap">
                <img src={iconCup} />
                <span className="hex__text">Snack</span>
            </div>
        </div>
    )
}

export default Hex