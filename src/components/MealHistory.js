import React from 'react'
import './MealHistory.css'

import imgM01 from '../assets/images/m01.png'
import imgL02 from '../assets/images/l02.png'
import imgL03 from '../assets/images/l03.png'
import imgD01 from '../assets/images/d01.png'
import imgD02 from '../assets/images/d02.png'
import imgS01 from '../assets/images/s01.png'
import imgS02 from '../assets/images/s02.png'

import iconScroll from '../assets/icons/component_scroll.svg'

function MealHistory() {
    return (
        <div className="meal__container">
            <div className="meal__wrap">
                <div className="meal__content">
                    <div className="meal__title-row1">05.21.Morning</div>
                    <img src={imgM01} alt="" />
                </div>
                <div className="meal__content">
                    <div className="meal__title-row1">05.21.Lunch</div>
                    <img src={imgL03} alt="" /> 
                </div>
                <div className="meal__content">
                    <div className="meal__title-row1">05.21.Dinner</div>
                    <img src={imgD01} alt="" />
                </div>
                <div className="meal__content">
                    <div className="meal__title-row1">05.21.Snack</div>
                    <img src={imgS02} alt="" />
                </div>
                <div className="meal__content">
                    <div className="meal__title-row2">05.21.Morning</div>
                    <img src={imgM01} alt="" />
                </div>
                <div className="meal__content">
                    <div className="meal__title-row2">05.21.Lunch</div>
                    <img src={imgL02} alt="" />
                </div>
                <div className="meal__content">
                    <div className="meal__title-row2">05.21.Dinner</div>
                    <img src={imgD02} alt="" />
                </div>
                <div className="meal__content">
                    <div className="meal__title-row2">05.21.Snack</div>
                    <img src={imgS01} alt="" />
                </div>

                <div className="meal__btn-scroll">
                    <img src={iconScroll} alt="" />
                </div>
                <div className="meal__btn">
                    <span>記録をもっと見る</span>
                </div>
            </div>
            
        </div>
    )
}

export default MealHistory