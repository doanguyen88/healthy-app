import React from 'react'
import './Banner.css'

import imgMain from '../assets/images/main_photo.png'
import imgGraph from '../assets/images/main_graph.png'


function Banner() {
    return (
        <div className="banner__container">
            <div className="banner__main">
                <img className="main__photo" src={imgMain} />
            </div>
            <div className="banner__chart">
                <img className="chart__photo" src={imgGraph} />
            </div>
        </div>
    )
}

export default Banner