import React from 'react'
import './Header.css'

import iconLogo from '../assets/icons/logo.svg'
import iconMemo from '../assets/icons/memo.svg'
import iconChallenge from '../assets/icons/challenge.svg'
import iconInfo from '../assets/icons/info.svg'
import iconMenu from '../assets/icons/menu.svg'


function Header() {
    return (
        <div className="header">
            <img className="header__logo" src={iconLogo} alt='logo' />
            <div className="header__right">
                <a>
                    <img src={iconMemo} />
                    <span>自分の記録</span>
                </a>
                <a>
                    <img src={iconChallenge} />
                    <span>チャレンジ</span>
                </a>
                <a>
                    <img src={iconInfo} />
                    <span>お知らせ</span>
                    <div className="info__count">
                        <span className="info__count-text">1</span>
                    </div>
                </a>
                <a>
                    <img className="header__menu" src={iconMenu} />
                </a>
            </div>
        </div>
    )
}

export default Header