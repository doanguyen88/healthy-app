import React from "react";
import "./Record.css";

import imgRecord1 from "../assets/images/MyRecommend-1.png";
import imgRecord2 from "../assets/images/MyRecommend-2.png";
import imgRecord3 from "../assets/images/MyRecommend-3.png";

import imgBodyRecord from "../assets/images/body_record.png";
import iconScroll from "../assets/icons/component_scroll.svg";

function Record() {
  return (
    <div className="record__container">
      <div className="record__wrap">
        <div className="record__content">
          <img src={imgRecord1} />
          <div className="record__btn-record">
            <span className="record__text-title">BODY RECORD</span>
            <span className="record__text-item">自分のカラダの記録</span>
          </div>
        </div>
        <div className="record__content">
          <img src={imgRecord2} />
          <div className="record__btn-record">
            <span className="record__text-title">MY EXERCISE</span>
            <span className="record__text-item">自分の運動の記録</span>
          </div>
        </div>
        <div className="record__content">
          <img src={imgRecord3} />
          <div className="record__btn-record">
            <span className="record__text-title">MY DIARY</span>
            <span className="record__text-item">自分の日記</span>
          </div>
        </div>
      </div>

      <div className="record__detail">
        <div className="body__record">
          <img src={imgBodyRecord} />
        </div>

        <div className="my__exercise">
            <div className="my__exercise-title">
                <div className="my__exercise-text">MY EXERCISE</div>
                <div className="my__exercise-time">2021.05.21</div>
            </div>
            {myExercise.map((ex) => {
                return (
                <div className="my__exercise-list">
                    <div className="my__exercise-content">
                        <span className="my__exercise-practice">{ex.title}</span>
                        <span className="my__exercise-duration">{ex.duration}</span>
                    </div>
                    <div className="my__exercise-kcal">{ex.kcal}</div>
                </div>
                );
            })}
        </div>

        <div className="record__btn-scroll">
          <img src={iconScroll} alt="" />
        </div>
        <div className="my__diary">
          <span className="mydiary__title">MY DIARY</span>
          <div className="record__mydiary-detail">
            <div className="mydiary__content">
              <div className="mydiary__time">2021.05.21 23:25</div>
              <div className="mydiary__text">
                <span>私の日記の記録が一部表示されます。</span>
                <span>
                  テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト…
                </span>
              </div>
            </div>
            <div className="mydiary__content">
              <div className="mydiary__time">2021.05.21 23:25</div>
              <div className="mydiary__text">
                私の日記の記録が一部表示されます。
                テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト…
              </div>
            </div>
            <div className="mydiary__content">
              <div className="mydiary__time">2021.05.21 23:25</div>
              <div className="mydiary__text">
                私の日記の記録が一部表示されます。
                テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト…
              </div>
            </div>
            <div className="mydiary__content">
              <div className="mydiary__time">2021.05.21 23:25</div>
              <div className="mydiary__text">
                私の日記の記録が一部表示されます。
                テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト…
              </div>
            </div>
            <div className="mydiary__content">
              <div className="mydiary__time">2021.05.21 23:25</div>
              <div className="mydiary__text">
                私の日記の記録が一部表示されます。
                テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト…
              </div>
            </div>
            <div className="mydiary__content">
              <div className="mydiary__time">2021.05.21 23:25</div>
              <div className="mydiary__text">
                私の日記の記録が一部表示されます。
                テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト…
              </div>
            </div>
            <div className="mydiary__content">
              <div className="mydiary__time">2021.05.21 23:25</div>
              <div className="mydiary__text">
                私の日記の記録が一部表示されます。
                テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト…
              </div>
            </div>
            <div className="mydiary__content">
              <div className="mydiary__time">2021.05.21 23:25</div>
              <div className="mydiary__text">
                私の日記の記録が一部表示されます。
                テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト…
              </div>
            </div>
          </div>
        </div>
        <div className="record__btn">
          <span>記録をもっと見る</span>
        </div>
      </div>
    </div>
  );
}

export default Record;

const myExercise = [
  {
    title: "家事全般（立位・軽い）",
    duration: "10 min",
    kcal: "26kcal",
  },
  {
    title: "家事全般（立位・軽い）",
    duration: "10 min",
    kcal: "26kcal",
  },
  {
    title: "家事全般（立位・軽い）",
    duration: "10 min",
    kcal: "26kcal",
  },
  {
    title: "家事全般（立位・軽い）",
    duration: "10 min",
    kcal: "26kcal",
  },
  {
    title: "家事全般（立位・軽い）",
    duration: "10 min",
    kcal: "26kcal",
  },
];
