import React from 'react'
import './Footer.css'


function Footer() {
    return (
        <div className="footer__container">
            <div className="footer__content">
                <a><span>会員登録</span></a>
                <a><span>運営会社</span></a>
                <a><span>利用規約</span></a>
                <a><span>個人情報の取扱について</span></a>
                <a><span>特定商取引法に基づく表記</span></a>
                <a><span>お問い合わせ</span></a>
            </div>
        </div>
    )
}

export default Footer