import React from 'react'
import './Home.css'
import Banner from './Banner'
import Hex from './Hex'
import MealHistory from './MealHistory'
import Record from './Record'


function Home() {
    return (
        <div className="home">
            <Banner />
            <Hex />
            <MealHistory />
            <Record />
        </div>
    )
}

export default Home